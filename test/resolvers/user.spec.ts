import { assert, expect } from 'chai';
import * as faker from 'faker';
import 'mocha';
import * as mongoose from 'mongoose';
import * as uuid from 'uuid/v4';
import { IAppConfig } from '../../src/app';
import { IModels, Models } from '../../src/models';
import { IResolvers, Resolvers } from '../../src/resolvers';
import TestDbHelper from '../helpers/db';

const configs: IAppConfig = {
    Cron: {},
    JWT: {
        secret: Buffer.from(Math.random().toString(36)).toString('base64'),
    },
    Name: 'Backend Template',
    Port: 3000,
    Redis: {
        enabled: false,
    },
    Timezone: 'UTC',
    Type: 'API',
    Version: '1.0.0',
};
const db = new TestDbHelper();
let models: IModels;
let resolvers: IResolvers;

describe('Resolvers:User', () => {
    before(() => {
        return db.start().then(() => {
            return db.getConnectionString().then((uri) => {
                return Models(uri).then((m) => {
                    models = m;
                    resolvers = Resolvers(configs, models);
                });
            });
        });
    });
    after(() => mongoose.disconnect().then(() => db.stop()));
    afterEach(() => Promise.all([
        models.role.deleteMany({}),
        models.user.deleteMany({}),
    ]));
    describe('#create()', () => {
        it('should create a user because the account already exists', () => {
            return resolvers.user.create({
                email: faker.internet.email(),
                first_name: faker.name.firstName(),
                last_name: faker.name.lastName(),
                middle_name: '',
                role: null,
                username: faker.internet.userName(),
                verified: true,
            }).then((user) => {
                return resolvers.user.create({
                    email: faker.internet.email(),
                    first_name: faker.name.firstName(),
                    last_name: faker.name.lastName(),
                    middle_name: '',
                    role: null,
                    username: user.username,
                    verified: true,
                }).then(() => {
                    assert.fail('The user should not have been created');
                }).catch((err) => {
                    expect(err).to.have.property('message', 'A user already exists by the specified username');
                    expect(err).to.have.property('statusCode', 409);
                });
            });
        });
    });
    describe('#getByUsername()', () => {
        it('should get user by username', () => {
            return resolvers.user.create({
                email: faker.internet.email(),
                first_name: faker.name.firstName(),
                last_name: faker.name.lastName(),
                middle_name: '',
                role: null,
                username: faker.internet.userName(),
                verified: true,
            }).then((user) => {
                return resolvers.user.getByUsername(user.username);
            });
        });
    });
    describe('#isAuthorized()', () => {
        it('should check that the user is authorized', () => {
            return resolvers.role.create({
                name: 'Admin',
                permissions: [],
            }).then((role) => {
                return resolvers.user.create({
                    email: faker.internet.email(),
                    first_name: faker.name.firstName(),
                    last_name: faker.name.lastName(),
                    middle_name: '',
                    role: role.id,
                    username: faker.internet.userName(),
                    verified: true,
                }).then((user) => {
                    return resolvers.user.isAuthorized(user.id);
                });
            });
        });
        it('should check that the user is not authorized because of their role', () => {
            return resolvers.user.create({
                email: faker.internet.email(),
                first_name: faker.name.firstName(),
                last_name: faker.name.lastName(),
                middle_name: '',
                role: null,
                username: faker.internet.userName(),
                verified: true,
            }).then((user) => {
                return resolvers.user.isAuthorized(user.id).then(() => {
                    assert.fail('User should not have been authorized');
                }).catch((err) => {
                    expect(err).to.have.property('message', 'Your username/password combination is incorrect');
                    expect(err).to.have.property('statusCode', 403);
                });
            });
        });
        it('should check that the user is not authorized because of the user', () => {
            return resolvers.user.isAuthorized(uuid()).then(() => {
                assert.fail('User should not have been authorized');
            }).catch((err) => {
                expect(err).to.have.property('message', 'No user found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
    });
    describe('#list()', () => {
        it('should get a list of users', () => {
            return resolvers.user.list({}, {
                limit: 25,
                page: 1,
                sort: [['name', 'ASC']],
            });
        });
    });
    describe('#update()', () => {
        it('should update the role of the user', () => {
            return resolvers.role.create({
                name: 'Admin',
                permissions: [],
            }).then((role) => {
                return resolvers.user.create({
                    email: faker.internet.email(),
                    first_name: faker.name.firstName(),
                    last_name: faker.name.lastName(),
                    middle_name: '',
                    role: role.id,
                    username: faker.internet.userName(),
                    verified: true,
                }).then((oldUser) => {
                    return resolvers.user.update(oldUser.id, {
                        role: null,
                        verified: true,
                    }).then((newUser) => {
                        expect(newUser).to.have.property('role', null);
                    });
                });
            });
        });
        it('should not update the user', () => {
            return resolvers.user.update(uuid(), {
                verified: false,
            }).then(() => {
                assert.fail('User should not have been updated');
            }).catch((err) => {
                expect(err).to.have.property('message', 'No user found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
    });
});
