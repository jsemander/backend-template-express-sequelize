import { assert, expect } from 'chai';
import 'mocha';
import * as mongoose from 'mongoose';
import { IAppConfig } from '../../src/app';
import { IModels, Models } from '../../src/models';
import { IResolvers, Resolvers } from '../../src/resolvers';
import TestDbHelper from '../helpers/db';

const configs: IAppConfig = {
    Cron: {},
    JWT: {
        secret: Buffer.from(Math.random().toString(36)).toString('base64'),
    },
    Name: 'Backend Template',
    Port: 3000,
    Redis: {
        enabled: false,
    },
    Timezone: 'UTC',
    Type: 'API',
    Version: '1.0.0',
};
const db = new TestDbHelper();
let models: IModels;
let resolvers: IResolvers;

describe('Resolvers:Config', () => {
    before(() => {
        return db.start().then(() => {
            return db.getConnectionString().then((uri) => {
                return Models(uri).then((m) => {
                    models = m;
                    resolvers = Resolvers(configs, models);
                });
            });
        });
    });
    after(() => mongoose.disconnect().then(() => db.stop()));
    afterEach(() => models.config.deleteMany({}));
    describe('#getByName()', () => {
        it('should retrieve a config by name successfully', () => {
            const config = new models.config({
                name: 'Name',
                value: configs.Name,
            });
            return config.save().then(() => {
                return resolvers.config.getByName('Name').then((record) => {
                    expect(record).to.have.property('name', 'Name');
                    expect(record).to.have.property('value', configs.Name);
                });
            });
        });
        it('should not retrieve a config by name successfully', () => {
            return resolvers.config.getByName('Name').then(() => {
                assert.fail('Config should not have been found');
            }).catch((err) => {
                expect(err).to.have.property('message', 'No config found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
    });
    describe('#list()', () => {
        it('should retrieve a list of configs successfully', () => {
            return resolvers.config.list({});
        });
    });
});
