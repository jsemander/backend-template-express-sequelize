import { Model, ModelAttributes, STRING, UUID } from 'sequelize';
import * as uuid from 'uuid/v4';

export const PermissionSchema: ModelAttributes = {
    id: {
        allowNull: false,
        defaultValue: () => uuid(),
        primaryKey: true,
        type: UUID,
    },
    level: {
        allowNull: false,
        type: STRING,
    },
    model: {
        allowNull: false,
        type: STRING,
    },
    name: {
        allowNull: false,
        type: STRING,
    },
};
export interface IPermission {
    [key: string]: any;
    createdAt: string;
    id: string;
    level: string;
    model: string;
    name: string;
    updatedAt: string;
}
export class PermissionModel extends Model {
    public readonly id!: string;
    public level!: string;
    public model!: string;
    public name!: string;
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}
