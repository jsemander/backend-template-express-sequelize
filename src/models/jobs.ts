import { BOOLEAN, DATE, JSON, Model, ModelAttributes, STRING, UUID } from 'sequelize';
import * as uuid from 'uuid/v4';

export const JobSchema: ModelAttributes = {
    action: {
        allowNull: false,
        type: STRING,
    },
    data: {
        allowNull: false,
        defaultValue: {},
        type: JSON,
    },
    enabled: {
        allowNull: false,
        defaultValue: true,
        type: BOOLEAN,
    },
    id: {
        allowNull: false,
        defaultValue: () => uuid(),
        primaryKey: true,
        type: UUID,
    },
    lastFinishedAt: {
        defaultValue: null,
        type: DATE,
    },
    lastModifiedBy: {
        defaultValue: null,
        type: STRING,
    },
    lastRunAt: {
        defaultValue: null,
        type: DATE,
    },
    model: {
        allowNull: false,
        type: STRING,
    },
    name: {
        allowNull: false,
        type: STRING,
    },
    nextRunAt: {
        defaultValue: null,
        type: DATE,
    },
    repeatInterval: {
        defaultValue: null,
        type: STRING,
    },
    repeatTimezone: {
        allowNull: false,
        defaultValue: 'America/New_York',
        type: STRING,
    },
};
export interface IJob {
    [key: string]: any;
    action: string;
    createdAt: string;
    data: {
        [key: string]: any;
    };
    enabled: boolean;
    id: string;
    lastFinishedAt: string;
    lastModifiedBy: string;
    model: string;
    name: string;
    nextRunAt: string;
    repeatInterval: string;
    repeatTimezone: string;
    updatedAt: string;
}
export class JobModel extends Model {
    public action!: string;
    public data!: {
        [key: string]: any;
    };
    public enabled!: boolean;
    public readonly id!: string;
    public lastFinishedAt!: Date;
    public lastModifiedBy!: Date;
    public model!: string;
    public name!: string;
    public nextRunAt!: Date;
    public repeatInterval!: string;
    public repeatTimezone!: string;
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}
