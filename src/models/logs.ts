import { JSON, Model, ModelAttributes, STRING, UUID } from 'sequelize';
import * as uuid from 'uuid/v4';
import { LogUpdateModel } from './logUpdates';
import { UserModel } from './users';

export const LogSchema: ModelAttributes = {
    action: {
        allowNull: false,
        type: STRING,
    },
    data: {
        allowNull: false,
        defaultValue: {},
        type: JSON,
    },
    entity: {
        allowNull: false,
        defaultValue: {},
        type: JSON,
    },
    entityId: {
        type: UUID,
    },
    error: {
        allowNull: false,
        defaultValue: {},
        type: JSON,
    },
    id: {
        allowNull: false,
        defaultValue: () => uuid(),
        primaryKey: true,
        type: UUID,
    },
    level: {
        allowNull: false,
        defaultValue: 'data',
        type: STRING,
        values: ['data', 'system'],
    },
    model: {
        allowNull: false,
        type: STRING,
        values: ['config', 'job', 'role', 'session', 'user'],
    },
    type: {
        allowNull: false,
        defaultValue: 'info',
        type: STRING,
        values: ['error', 'info'],
    },
};
export class LogModel extends Model {
    public readonly action!: string;
    public readonly createdAt!: Date;
    public readonly data!: { [key: string]: any; };
    public entity?: { [key: string]: any; };
    public readonly entityId?: string;
    public readonly error!: { [key: string]: any; };
    public readonly id!: string;
    public readonly level!: 'data' | 'system';
    public readonly model!: 'config' | 'job' | 'role' | 'session' | 'user';
    public readonly type!: 'error' | 'info';
    public readonly updatedAt!: Date;
    public readonly updates!: LogUpdateModel[];
    public user?: UserModel | { name: string };
    public userId!: string;
}
