import { BOOLEAN, Model, ModelAttributes, STRING, UUID } from 'sequelize';
import * as uuid from 'uuid/v4';
import { RolePermissionModel } from './rolesPermissions';

export const RoleSchema: ModelAttributes = {
    enabled: {
        allowNull: false,
        defaultValue: true,
        type: BOOLEAN,
    },
    id: {
        allowNull: false,
        defaultValue: () => uuid(),
        primaryKey: true,
        type: UUID,
    },
    name: {
        allowNull: false,
        type: STRING,
    },
};
export class RoleModel extends Model {
    public enabled!: boolean;
    public readonly id!: string;
    public name!: string;
    public permissions!: RolePermissionModel[];
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}
