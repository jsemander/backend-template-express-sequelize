import { JSON, Model, ModelAttributes, STRING, UUID } from 'sequelize';
import * as uuid from 'uuid/v4';

export const ConfigSchema: ModelAttributes = {
    id: {
        allowNull: false,
        defaultValue: () => uuid(),
        primaryKey: true,
        type: UUID,
    },
    name: {
        allowNull: false,
        type: STRING,
    },
    value: {
        allowNull: false,
        type: JSON,
    },
};
export interface IConfig {
    [key: string]: any;
    createdAt: string;
    id: string;
    name: string;
    updatedAt: string;
    value: string;
}
export class ConfigModel extends Model {
    public readonly id!: string;
    public name!: string;
    public value!: string;
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}
