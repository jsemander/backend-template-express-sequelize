import { JSON, Model, ModelAttributes, STRING, UUID } from 'sequelize';
import * as uuid from 'uuid/v4';

export const LogUpdateSchema: ModelAttributes = {
    fieldName: {
        allowNull: false,
        type: STRING,
    },
    id: {
        allowNull: false,
        defaultValue: () => uuid(),
        primaryKey: true,
        type: UUID,
    },
    newValue: {
        type: JSON,
    },
    oldValue: {
        type: JSON,
    },
};
export interface ILogUpdate {
    [key: string]: any;
    fieldName: string;
    id: string;
    logId: string;
    newValue: any;
    oldValue: any;
}
export class LogUpdateModel extends Model {
    public readonly fieldName!: string;
    public readonly id!: string;
    public readonly logId!: string;
    public readonly newValue!: any;
    public readonly oldValue!: any;
}
