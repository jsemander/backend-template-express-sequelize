import { BOOLEAN, Model, ModelAttributes, UUID } from 'sequelize';
import * as uuid from 'uuid/v4';
import { PermissionModel } from './permissions';
import { RoleModel } from './roles';

export const RolePermissionSchema: ModelAttributes = {
    access: {
        allowNull: false,
        defaultValue: false,
        type: BOOLEAN,
    },
    create: {
        allowNull: false,
        defaultValue: false,
        type: BOOLEAN,
    },
    id: {
        allowNull: false,
        defaultValue: () => uuid(),
        primaryKey: true,
        type: UUID,
    },
    modify: {
        allowNull: false,
        defaultValue: false,
        type: BOOLEAN,
    },
};
export class RolePermissionModel extends Model {
    public access!: boolean;
    public create!: boolean;
    public readonly id!: string;
    public modify!: boolean;
    public readonly permissionId!: string;
    public readonly permission?: PermissionModel;
    public readonly roleId!: string;
    public readonly role?: RoleModel;
    public readonly createdAt!: string;
    public readonly updatedAt!: string;
}
