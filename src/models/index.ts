import { ModelCtor, Sequelize } from 'sequelize';
import { ConfigModel, ConfigSchema } from './configs';
import { JobModel, JobSchema } from './jobs';
import { LogModel, LogSchema } from './logs';
import { LogUpdateModel, LogUpdateSchema } from './logUpdates';
import { PermissionModel, PermissionSchema } from './permissions';
import { RoleModel, RoleSchema } from './roles';
import { RolePermissionModel, RolePermissionSchema } from './rolesPermissions';
import { SessionModel, SessionSchema } from './sessions';
import { UserModel, UserSchema } from './users';

export interface IModels {
    config: ModelCtor<ConfigModel>;
    job: ModelCtor<JobModel>;
    log: ModelCtor<LogModel>;
    logUpdate: ModelCtor<LogUpdateModel>;
    permission: ModelCtor<PermissionModel>;
    role: ModelCtor<RoleModel>;
    rolePermission: ModelCtor<RolePermissionModel>;
    session: ModelCtor<SessionModel>;
    user: ModelCtor<UserModel>;
}
export const Models = (dbUrl: string): Promise<IModels> => {
    const sequelize = new Sequelize(dbUrl, {
        dialectOptions: {
            useUTC: true,
        },
        logging: process.env.DEBUG === 'yes',
        timezone: 'UTC',
    });
    return new Promise((resolve, reject) => {
        return sequelize.authenticate().then(() => {
            sequelize.define('configs', ConfigSchema, {
                underscored: true,
            });
            sequelize.define('jobs', JobSchema, {
                underscored: true,
            });
            sequelize.define('logs', LogSchema, {
                underscored: true,
            });
            sequelize.define('log_updates', LogUpdateSchema, {
                timestamps: false,
                underscored: true,
            });
            sequelize.define('permissions', PermissionSchema, {
                underscored: true,
            });
            sequelize.define('roles', RoleSchema, {
                underscored: true,
            });
            sequelize.define('roles_permissions', RolePermissionSchema, {
                underscored: true,
            });
            sequelize.define('sessions', SessionSchema, {
                underscored: true,
            });
            sequelize.define('users', UserSchema, {
                defaultScope: {
                    attributes: {
                        exclude: ['password'],
                    },
                },
                underscored: true,
            });
            const models = {
                config: sequelize.model('configs') as ModelCtor<ConfigModel>,
                job: sequelize.model('jobs') as ModelCtor<JobModel>,
                log: sequelize.model('logs') as ModelCtor<LogModel>,
                logUpdate: sequelize.model('log_updates') as ModelCtor<LogUpdateModel>,
                permission: sequelize.model('permissions') as ModelCtor<PermissionModel>,
                role: sequelize.model('roles') as ModelCtor<RoleModel>,
                rolePermission: sequelize.model('roles_permissions') as ModelCtor<RolePermissionModel>,
                session: sequelize.model('sessions') as ModelCtor<SessionModel>,
                user: sequelize.model('users') as ModelCtor<UserModel>,
            };
            models.session.belongsTo(models.user, {
                as: 'user',
                onDelete: 'RESTRICT',
            });
            models.log.belongsTo(models.user, {
                as: 'user',
                onDelete: 'RESTRICT',
            });
            models.log.hasMany(models.logUpdate, {
                as: 'updates',
                onDelete: 'RESTRICT',
            });
            models.logUpdate.belongsTo(models.log, {
                as: 'log',
                onDelete: 'RESTRICT',
            });
            models.rolePermission.belongsTo(models.permission, {
                as: 'permission',
                onDelete: 'RESTRICT',
            });
            models.rolePermission.belongsTo(models.role, {
                as: 'role',
                onDelete: 'RESTRICT',
            });
            models.role.hasMany(models.rolePermission, {
                as: 'permissions',
                onDelete: 'RESTRICT',
            });
            models.role.hasMany(models.user, {
                as: 'users',
                onDelete: 'RESTRICT',
            });
            models.user.belongsTo(models.role, {
                as: 'role',
                onDelete: 'RESTRICT',
            });
            return sequelize.sync({
                alter: true,
                // force: true,
            }).then(() => {
            //     return Promise.all([
            //         models.config.create<ConfigModel>({
            //             name: 'Authentication',
            //             value: {
            //                 maxLockoutAge: 30,
            //                 maxLoginAttempts: 3,
            //                 salt: Buffer.from(Math.random().toString(36)).toString('base64'),
            //                 saltOrRounds: 10,
            //             },
            //         }),
            //         models.config.create<ConfigModel>({
            //             name: 'JWT',
            //             value: {
            //                 secret: Buffer.from(Math.random().toString(36)).toString('base64'),
            //             },
            //         }),
            //         models.config.create<ConfigModel>({
            //             name: 'Password',
            //             value: {
            //                 maxAge: 90,
            //                 maxHistory: 5,
            //                 minLength: 8,
            //             },
            //         }),
            //         models.config.create<ConfigModel>({
            //             name: 'Timezone',
            //             value: 'UTC',
            //         }),
            //     ]);
            // }).then(() => {
            //     return Promise.all([
            //         models.permission.create<PermissionModel>({
            //             enabled: true,
            //             level: 'all',
            //             model: 'user',
            //             name: 'Users',
            //         }),
            //     ]);
            // }).then((permissions) => {
            //     return Promise.all([
            //         models.role.create<RoleModel>({
            //             enabled: true,
            //             name: 'User',
            //             permissions: permissions.map((permission) => {
            //                 return {
            //                     access: false,
            //                     create: false,
            //                     modify: false,
            //                     permissionId: permission.id,
            //                 };
            //             }),
            //         }, {
            //             include: [{
            //                 as: 'permissions',
            //                 model: models.rolePermission,
            //             }],
            //         }),
            //         models.role.create<RoleModel>({
            //             enabled: true,
            //             name: 'Admin',
            //             permissions: permissions.map((permission) => {
            //                 return {
            //                     access: true,
            //                     create: true,
            //                     modify: true,
            //                     permissionId: permission.id,
            //                 };
            //             }),
            //         }, {
            //             include: [{
            //                 as: 'permissions',
            //                 model: models.rolePermission,
            //             }],
            //         }),
            //     ]);
            // }).then(() => {
                return resolve(models);
            });
        }).catch(reject);
    });
};
