import { DATE, Model, ModelAttributes, STRING, UUID } from 'sequelize';
import * as uuid from 'uuid/v4';
import { UserModel } from './users';

export const SessionSchema: ModelAttributes = {
    expiration: {
        allowNull: false,
        type: DATE,
    },
    id: {
        allowNull: false,
        defaultValue: () => uuid(),
        primaryKey: true,
        type: UUID,
    },
    socket: {
        type: STRING,
    },
};
export class SessionModel extends Model {
    public expiration!: Date;
    public readonly id!: string;
    public socket!: string;
    public user!: UserModel;
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}
