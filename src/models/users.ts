import { BOOLEAN, Model, ModelAttributes, STRING, UUID, VIRTUAL } from 'sequelize';
import * as uuid from 'uuid/v4';
import { RoleModel } from './roles';

export const UserSchema: ModelAttributes = {
    email: {
        allowNull: false,
        type: STRING,
    },
    enabled: {
        allowNull: false,
        defaultValue: true,
        type: BOOLEAN,
    },
    firstName: {
        allowNull: false,
        type: STRING,
    },
    id: {
        allowNull: false,
        defaultValue: () => uuid(),
        primaryKey: true,
        type: UUID,
    },
    lastName: {
        allowNull: false,
        type: STRING,
    },
    name: {
        get(this: UserModel): string {
            return this.getDataValue('firstName') + ' ' + this.getDataValue('lastName');
        },
        type: VIRTUAL,
    },
    password: {
        type: STRING,
    },
    verified: {
        allowNull: false,
        defaultValue: true,
        type: BOOLEAN,
    },
};
export class UserModel extends Model {
    public email!: string;
    public enabled!: boolean;
    public firstName!: string;
    public readonly id!: string;
    public lastName!: string;
    public readonly name!: string;
    public password!: string;
    public role!: RoleModel;
    public verified!: boolean;
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}
