import * as moment from 'moment';
import { Resolver } from '../lib/Resolver';
import { IDateRange } from '../lib/Types';
import { IModels } from '../models';
import { SessionModel } from '../models/sessions';
import { UserModel } from '../models/users';

export interface ISessionRemoveParams {
    expiration?: IDateRange;
    id?: string;
    userId?: string;
}
export interface ISessionUpdateParams {
    expiration?: string;
    socket?: string;
    userId?: string;
}
export class Session extends Resolver<IModels> {
    /**
     * Create session
     */
    public create(userId: string, socketId?: string): Promise<SessionModel> {
        return this.removeByUser(userId).then(() => {
            return this.models.session.create<SessionModel>({
                expiration: moment().add({ day: 1 }).toISOString(),
                socket: socketId,
                userId,
            });
        });
    }
    
    /**
     * Get session by id
     */
    public get(id: string): Promise<SessionModel> {
        return this.models.session.findByPk<SessionModel>(id, {
            include: [{
                as: 'user',
                include: [{
                    as: 'role',
                    include: [{
                        as: 'permissions',
                        model: this.models.rolePermission,
                    }],
                    model: this.models.role,
                }],
                model: this.models.user,
            }],
        }).then((session) => {
            if (!session) {
                return Promise.reject<any>({
                    message: 'There was no session record found',
                    statusCode: 404,
                });
            }
            if (!moment(session.expiration).isAfter(moment())) {
                return this.removeById(id).then(() => {
                    return Promise.reject<any>({
                        message: 'The session is expired',
                        statusCode: 409,
                    });
                });
            }
            return session;
        });
    }
    
    /**
     * Get session by socket id
     */
    public getBySocketId(socketId: string): Promise<SessionModel | null> {
        return this.models.session.findOne<SessionModel>({
            include: [{
                as: 'user',
                include: [{
                    as: 'role',
                    model: this.models.role,
                }],
                model: this.models.user,
            }],
            where: { socket: socketId },
        });
    }
    
    /**
     * Get session by user id
     */
    public getByUserId(userId: string): Promise<SessionModel | null> {
        return this.models.session.findOne<SessionModel>({
            include: [{
                as: 'user',
                include: [{
                    as: 'role',
                    model: this.models.role,
                }],
                model: this.models.user,
            }],
            where: { user: userId },
        });
    }
    
    /**
     * Delete session by params
     */
    public remove(params: ISessionRemoveParams): Promise<boolean> {
        return this.models.session.findAll<SessionModel>({
            where: Object.assign(params),
        }).then((sessions) => {
            return sessions.reduce((chain, session) => {
                return chain.then(() => {
                    return session.destroy();
                });
            }, Promise.resolve());
        }).then(() => true);
    }
    
    /**
     * Delete session by id
     */
    public removeById(id: string): Promise<boolean> {
        return this.remove({ id });
    }
    
    /**
     * Delete session by user id
     */
    public removeByUser(userId: string): Promise<boolean> {
        return this.remove({ userId });
    }
    
    /**
     * Update session
     */
    public update(id: string, data: ISessionUpdateParams): Promise<SessionModel> {
        return this.get(id).then((session) => {
            // Set the session, return the session after saving
            return session.update(data);
        });
    }
}
