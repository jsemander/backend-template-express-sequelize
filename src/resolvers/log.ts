import { paginateRecords } from '../lib/Pagination';
import { Resolver } from '../lib/Resolver';
import { IDateRange, IPagination, IPaginationOptions } from '../lib/Types';
import { IModels } from '../models';
import { LogModel } from '../models/logs';

export interface ILogCreateParams {
    action: string;
    data?: any;
    entity?: { [key: string]: any; };
    entityId?: string;
    model: string;
    error?: any;
    level?: string;
    type: string;
    updates: Array<{
        fieldName: string;
        newValue: any;
        oldValue: any;
    }>;
    userId?: string;
}
export interface ILogListParams {
    action?: string;
    createdAt: IDateRange;
    entityId?: string;
    model?: string;
    level?: string;
    type?: string;
}
export class Log extends Resolver<IModels> {
    /**
     * Create log
     */
    public create(data: ILogCreateParams): Promise<LogModel> {
        switch (data.model) {
            case 'api':
            case 'cron':
            case 'email':
            case 'job':
            case 'system':
                data.level = 'system';
                break;
            default:
                data.level = 'data';
        }
        return this.models.log.create<LogModel>(data, {
            include: [{
                as: 'updates',
                model: this.models.logUpdate,
            }],
        });
    }
    
    /**
     * Get log by id
     */
    public get(id: string): Promise<LogModel> {
        return this.models.log.findByPk<LogModel>(id).then((log) => {
            if (!log) {
                return Promise.reject<any>({
                    message: 'No log found',
                    statusCode: 404,
                });
            }
            return log;
        });
    }
    
    /**
     * Retrieves a list of logs by params
     */
    public list(params: ILogListParams, options: IPaginationOptions): Promise<IPagination<LogModel>> {
        return paginateRecords<LogModel>(this.models.log, params, [{
            as: 'user',
            model: this.models.user,
        }], options.sort, options.page, options.limit);
    }
}
