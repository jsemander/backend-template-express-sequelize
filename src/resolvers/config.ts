import { Resolver } from '../lib/Resolver';
import { IModels } from '../models';
import { ConfigModel } from '../models/configs';

export class Config extends Resolver<IModels> {
    /**
     * Get config by name
     */
    public getByName(name: string): Promise<ConfigModel> {
        return this.models.config.findOne<ConfigModel>({
            where: { name },
        }).then((config) => {
            if (!config) {
                return Promise.reject<any>({
                    message: 'No config found',
                    statusCode: 404,
                });
            }
            return config;
        });
    }
    
    /**
     * Retrieves a list of configs by params
     */
    public list(params: any): Promise<ConfigModel[]> {
        return this.models.config.findAll<ConfigModel>({
            where: params,
        });
    }
}
