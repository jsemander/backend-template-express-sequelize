import { isEmpty } from 'lodash';
import * as sequelize from 'sequelize';
import { paginateRecords } from '../lib/Pagination';
import { Resolver } from '../lib/Resolver';
import { IPagination, IPaginationOptions } from '../lib/Types';
import { IModels } from '../models';
import { UserModel } from '../models/users';

export interface IUserCreateParams {
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    roleId: string | null;
    verified: boolean;
}
export interface IUserListParams {
    enabled?: boolean;
    roleId?: string | null;
    verified?: boolean;
}
export interface IUserUpdateParams {
    email?: string;
    enabled?: boolean;
    firstName?: string;
    lastName?: string;
    password?: string;
    roleId?: string | null;
    verified?: boolean;
}
export class User extends Resolver<IModels> {
    /**
     * Create user
     */
    public create(data: IUserCreateParams): Promise<UserModel> {
        return this.getByUsername(data.email).then((result) => {
            if (result) {
                return Promise.reject<any>({
                    message: 'A user already exists by the specified username',
                    statusCode: 409,
                });
            }
            return this.models.user.create<UserModel>(data);
        });
    }
    
    /**
     * Get user by id
     */
    public get(id: string): Promise<UserModel> {
        return this.models.user.findByPk<UserModel>(id, {
            include: [{
                as: 'role',
                include: [{
                    as: 'permissions',
                    include: [{
                        as: 'permission',
                        model: this.models.permission,
                    }],
                    model: this.models.rolePermission,
                }],
                model: this.models.role,
            }],
        }).then((user) => {
            if (!user) {
                return Promise.reject<any>({
                    message: 'No user found',
                    statusCode: 404,
                });
            }
            return user;
        });
    }
    
    /**
     * Get user by email
     */
    public getByEmail(email: string): Promise<UserModel | null> {
        return this.models.user.findOne<UserModel>({
            where: {
                email: {
                    [sequelize.Op.like]: email,
                },
            },
        });
    }
    
    /**
     * Get user by username
     */
    public getByUsername(username: string): Promise<UserModel | null> {
        return this.models.user.findOne<UserModel>({
            attributes: {
                include: ['password'],
            },
            where: {
                email: {
                    [sequelize.Op.like]: username,
                },
            },
        });
    }
    
    /**
     * Get user by id and validates that they are authorized
     */
    public isAuthorized(id: string, verbose: boolean = true): Promise<UserModel> {
        const defaultMessage = 'Your username/password combination is incorrect';
        return this.models.user.findByPk<UserModel>(id, {
            attributes: {
                include: ['password'],
            },
            include: [{
                as: 'role',
                include: [{
                    as: 'permissions',
                    include: [{
                        as: 'permission',
                        model: this.models.permission,
                    }],
                    model: this.models.rolePermission,
                }],
                model: this.models.role,
            }],
        }).then((user) => {
            if (!user) {
                return Promise.reject<any>({
                    message: verbose ? 'There was no user record found' : defaultMessage,
                    statusCode: 404,
                });
            }
            if (!user.enabled) {
                return Promise.reject<any>({
                    message: verbose ? 'Your user has been disabled' : defaultMessage,
                    statusCode: 404,
                });
            }
            if (!user.password) {
                return Promise.reject<any>({
                    message: verbose ? 'Your password has expired' : defaultMessage,
                    statusCode: 404,
                });
            }
            if (!user.role || !user.role.enabled || !user.role.permissions || !user.role.permissions.length) {
                return Promise.reject<any>({
                    message: verbose ? 'Your permission has been revoked' : defaultMessage,
                    statusCode: 404,
                });
            }
            return user;
        });
    }
    
    /**
     * Retrieves a list of users by params
     */
    public list(params: IUserListParams, options: IPaginationOptions): Promise<IPagination<UserModel>> {
        return paginateRecords<UserModel>(this.models.user, params, [{
            as: 'role',
            model: this.models.role,
        }], options.sort, options.page, options.limit);
    }
    
    /**
     * Update user
     */
    public update(id: string, data: IUserUpdateParams): Promise<UserModel> {
        return this.get(id).then((record) => {
            // Set the user, then return the user after saving
            return record.update(data);
        });
    }
}
