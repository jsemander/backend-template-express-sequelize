import * as parser from 'cron-parser';
import { isEmpty } from 'lodash';
import * as moment from 'moment-timezone';
import * as os from 'os';
import * as sequelize from 'sequelize';
import { paginateRecords } from '../lib/Pagination';
import { Resolver } from '../lib/Resolver';
import { IDateRange, IPagination, IPaginationOptions } from '../lib/Types';
import { IModels } from '../models';
import { JobModel } from '../models/jobs';

export interface IJobCreateParams {
    action: string;
    data?: any;
    model: string;
    name?: string;
    nextRunAt?: string;
    repeatInterval?: string;
    repeatTimezone?: string;
}
export interface IJobListParams {
    enabled?: boolean;
    nextRunAt?: IDateRange;
}
export interface IJobUpdateParams {
    action: string;
    data?: any;
    model: string;
    name?: string;
    nextRunAt?: string;
    repeatInterval?: string;
    repeatTimezone?: string;
}
export class Job extends Resolver<IModels> {
    /**
     * Create job
     */
    public create(data: IJobCreateParams): Promise<JobModel> {
        const params: any = {
            action: data.action,
            model: data.model,
            repeatInterval: data.repeatInterval,
        };
        if (!isEmpty(data.data)) {
            params.data = data.data;
        }
        return this.models.job.findOne<JobModel>({
            where: params,
        }).then((result) => {
            if (result) {
                return Promise.reject<any>({
                    message: 'A job already exists by the specified configuration',
                    statusCode: 409,
                });
            }
            return this.models.job.create<JobModel>(data);
        });
    }
    
    /**
     * Disables a job
     */
    public disable(id: string): Promise<JobModel> {
        return this.models.job.findByPk(id).then((job) => {
            if (!job) {
                return Promise.reject<any>({
                    message: 'No job found',
                    statusCode: 404,
                });
            }
            return job.update({ enabled: false });
        });
    }
    
    /**
     * Mark job as finished
     */
    public finish(id: string): Promise<JobModel | null> {
        return this.models.job.findByPk<JobModel>(id).then((job) => {
            if (!job) {
                return Promise.reject<any>({
                    message: 'No job found',
                    statusCode: 404,
                });
            }
            if (job.nextRunAt) {
                return job.update({ lastFinishedAt: moment().toISOString() });
            } else {
                return this.removeById(id).then(() => null);
            }
        });
    }

    /**
     * Get job by id
     */
    public get(id: string): Promise<JobModel | null> {
        return this.models.job.findByPk<JobModel>(id);
    }

    /**
     * Get next job to run
     */
    public getNext(): Promise<JobModel | null> {
        return this.models.job.findOne<JobModel>({
            order: [['nextRunAt', 'ASC']],
            where: {
                enabled: true,
                nextRunAt: {
                    [sequelize.Op.gte]: moment().toISOString(),
                },
            },
        });
    }

    /**
     * Retrieves a list of jobs by params
     */
    public list(params: IJobListParams, options: IPaginationOptions): Promise<IPagination<JobModel>> {
        return paginateRecords<JobModel>(this.models.job, params, [], options.sort, options.page, options.limit);
    }
    
    /**
     * Delete job by params
     */
    public removeById(id: string): Promise<boolean> {
        return this.models.job.findByPk<JobModel>(id).then((job) => {
            if (!job) {
                return Promise.reject<any>({
                    message: 'No job found',
                    statusCode: 404,
                });
            }
            return job.destroy().then(() => true);
        });
    }

    /**
     * Mark job as running
     */
    public running(id: string): Promise<JobModel> {
        return this.models.job.findByPk<JobModel>(id).then((job) => {
            if (!job) {
                return Promise.reject<any>({
                    message: 'No job found',
                    statusCode: 404,
                });
            }
            if (job.repeatInterval) {
                const options: { tz?: string; } = {};
                if (job.repeatTimezone) {
                    options.tz = job.repeatTimezone;
                }
                const interval = parser.parseExpression(job.repeatInterval, options);
                return job.update({
                    lastModifiedBy: `${os.hostname()} - ${process.pid}`,
                    lastRunAt: moment().toISOString(),
                    nextRunAt: interval.next().toISOString(),
                });
            } else {
                return job.update({
                    lastModifiedBy: `${os.hostname()} - ${process.pid}`,
                    lastRunAt: moment().toISOString(),
                    nextRunAt: null,
                });
            }
        });
    }
    
    /**
     * Update job by id
     */
    public update(id: string, data: IJobUpdateParams): Promise<JobModel> {
        return this.models.job.findOne<JobModel>({
            where: {
                action: data.action,
                id: { [sequelize.Op.not]: id },
                model: data.model,
                // repeatInterval: data.repeatInterval,
                // repeatTimezone: data.repeatTimezone,
            },
        }).then((result) => {
            if (result) {
                return Promise.reject<any>({
                    message: 'A job already exists by the specified configuration',
                    statusCode: 409,
                });
            }
            return this.get(id).then((record) => {
                // Check if the record was found
                if (!record) {
                    return Promise.reject<any>({
                        message: 'No job found',
                        statusCode: 404,
                    });
                }
                // Set the job, return the job after saving
                return record.update(data);
            });
        });
    }
}
