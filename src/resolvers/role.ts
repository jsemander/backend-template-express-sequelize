import * as sequelize from 'sequelize';
import { paginateRecords } from '../lib/Pagination';
import { Resolver } from '../lib/Resolver';
import { IPagination, IPaginationOptions } from '../lib/Types';
import { IModels } from '../models';
import { RoleModel } from '../models/roles';

export interface IRoleCreateParams {
    name: string;
    permissions?: Array<{
        access: boolean;
        create: boolean;
        modify: boolean;
        permissionId: string;
    }>;
}
export interface IRoleListParams {
    enabled?: boolean;
}
export interface IRoleUpdateParams {
    enabled?: boolean;
    name?: string;
    permissions?: Array<{
        access: boolean;
        create: boolean;
        modify: boolean;
        permissionId: string;
    }>;
}
export class Role extends Resolver<IModels> {
    /**
     * Create role
     */
    public create(data: IRoleCreateParams): Promise<RoleModel> {
        return this.models.role.findOne<RoleModel>({
            where: {
                name: {
                    [sequelize.Op.like]: data.name,
                },
            },
        }).then((result) => {
            if (result) {
                return Promise.reject<any>({
                    message: `A role already exists with the same name: ${result.name}`,
                    statusCode: 409,
                });
            }
            return this.models.role.create<RoleModel>(data, {
                include: [{
                    as: 'permissions',
                    model: this.models.rolePermission,
                }],
            });
        });
    }
    
    /**
     * Get role by id
     */
    public get(id: string): Promise<RoleModel> {
        return this.models.role.findByPk<RoleModel>(id, {
            include: [{
                as: 'permissions',
                include: [{
                    as: 'permission',
                    model: this.models.permission,
                }],
                model: this.models.rolePermission,
            }],
        }).then((role) => {
            if (!role) {
                return Promise.reject<any>({
                    message: 'No role found',
                    statusCode: 404,
                });
            }
            return role;
        });
    }
    
    /**
     * Get role by name
     */
    public getByName(name: string): Promise<RoleModel> {
        return this.models.role.findOne<RoleModel>({
            include: [{
                as: 'permissions',
                include: [{
                    as: 'permission',
                    model: this.models.permission,
                }],
                model: this.models.rolePermission,
            }],
            where: { name },
        }).then((role) => {
            if (!role) {
                return Promise.reject<any>({
                    message: 'No role found',
                    statusCode: 404,
                });
            }
            return role;
        });
    }
    
    /**
     * Retrieves a list of roles by params
     */
    public list(params: IRoleListParams, options: IPaginationOptions): Promise<IPagination<RoleModel>> {
        return paginateRecords<RoleModel>(this.models.role, params, [{
            as: 'permissions',
            include: [{
                as: 'permission',
                model: this.models.permission,
            }],
            model: this.models.rolePermission,
        }], options.sort, options.page, options.limit);
    }
    
    /**
     * Update role
     */
    public update(id: string, data: IRoleUpdateParams): Promise<RoleModel> {
        return this.models.role.findOne<RoleModel>({
            where: {
                id: { [sequelize.Op.not]: id },
                name: { [sequelize.Op.like]: data.name },
            },
        }).then((result) => {
            if (result) {
                return Promise.reject<any>({
                    message: `A role already exists with the same name: ${result.name}`,
                    statusCode: 409,
                });
            }
            return this.get(id).then((record) => {
                // Set the role, return the role after saving
                return record.update(data);
            });
        });
    }
}
