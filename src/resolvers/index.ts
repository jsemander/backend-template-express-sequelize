import { IAppConfig } from '../app';
import { JWT } from '../lib/JWT';
import { IModels } from '../models';
import { Config } from './config';
import { Job } from './job';
import { Log } from './log';
import { Role } from './role';
import { Session } from './session';
import { User } from './user';

export interface IResolvers {
    config: Config;
    job: Job;
    jwt: JWT;
    log: Log;
    role: Role;
    session: Session;
    user: User;
}
export const Resolvers = (configs: IAppConfig, models: IModels): IResolvers => {
    return {
        config: new Config(models),
        job: new Job(models),
        jwt: new JWT(configs.JWT),
        log: new Log(models),
        role: new Role(models),
        session: new Session(models),
        user: new User(models),
    };
};
