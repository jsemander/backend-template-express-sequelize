import { Express, NextFunction, Request, RequestHandler, Response, static as expressStatic } from 'express';
import { omit, pick } from 'lodash';
import * as moment from 'moment-timezone';
import { IAppConfig } from '../app';
import { IResolvers } from '../resolvers';
import Log from './logs';
import Role from './roles';
import Root from './root';
import User from './users';

/**
 * Validates that a user is logged in
 */
export const Authorization = (resolvers: IResolvers): RequestHandler => {
    return (req: Request, res: Response, next: NextFunction) => {
        const authHeader = req.header('Authorization');
        if (authHeader) {
            if (!authHeader.match(/^Bearer (.*?)\.(.*?)\.(.*?)$/i)) {
                return next({
                    message: 'Incorrect Authorization header format',
                    statusCode: 401,
                });
            }
            return resolvers.jwt.isAuthorized(authHeader.split(' ')[1]).then((sessionId) => {
                return resolvers.session.get(sessionId).then((session) => {
                    return resolvers.user.isAuthorized(session.user.id).then((user) => {
                        return resolvers.session.update(sessionId, {
                            expiration: moment().add({ minute: 30 }).toISOString(),
                        }).then(() => {
                            req.user = user;
                            return next();
                        });
                    });
                });
            }).catch(next);
        }
        return next({
            message: 'Please make sure your request has an Authorization header',
            statusCode: 401,
        });
    };
};
export const formatErrors: (error: {
    location: string;
    param: string;
    msg: any;
    value: any;
}) => any = ({ location, msg, param, value }) => msg;
export const Routes = (app: Express, configs: IAppConfig, resolvers: IResolvers): void => {
    /**
     * Implements robots.txt
     */
    app.use('/robots.txt', (req: Request, res: Response) => {
        return res.type('text/plain').send('UserAgent: *\nDisallow: /');
    });
    
    /**
     * Documentation End-point
     */
    if (process.env.NODE_ENV !== 'production') {
        app.use('/docs', expressStatic('docs'));
    }
    
    /**
     * All routes
     */
    app.use('/', Root(configs, resolvers));
    app.use('/logs', Authorization(resolvers), Log(configs, resolvers));
    app.use('/roles', Authorization(resolvers), Role(resolvers));
    app.use('/users', Authorization(resolvers), User(resolvers));
    
    /**
     * Base Route
     */
    app.use((req: Request, res: Response, next: NextFunction) => {
        return res.json(`${configs.Name} ${configs.Version}`);
    });
    
    /**
     * Define Error Handling
     */
    app.use((err: any, req: Request, res: Response, next: NextFunction) => {
        console.error(err);
        return resolvers.log.create({
            action: 'request',
            data: pick(Object.assign({}, req, {
                body: omit(req.body, [
                    'card_number',
                    'password',
                ]),
                connection: pick(req.connection, [
                    'remoteAddress',
                ]),
            }), [
                'body',
                'connection',
                'headers',
                'hostname',
                'protocol',
                'method',
                'query',
                'url',
            ]),
            entity: {
                name: 'Request Error',
            },
            error: err,
            model: 'api',
            type: 'error',
            updates: [],
        }).then(() => {
            return res.status(err.statusCode || 500).send(err);
        });
    });
};
