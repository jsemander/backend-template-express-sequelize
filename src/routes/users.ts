import { NextFunction, Request, Response, Router } from 'express';
import { body, validationResult } from 'express-validator/check';
import { compact, find, forEach, isEmpty, isEqual, map } from 'lodash';
import { formatErrors } from '.';
import { IPaginationOptions } from '../lib/Types';
import { booleanSanitizer } from '../lib/Validators';
import { IResolvers } from '../resolvers';
import { IUserListParams, IUserUpdateParams } from '../resolvers/user';

export default (resolvers: IResolvers) => {
    const router = Router();
    
    /**
     * @api {get} /users List Paginated Users
     * @apiGroup User
     * @apiName List Paginated Users
     * @apiSuccess {Object[]} users Array of users
     * @apiVersion 2.0.0
     */
    router.get('/', (req: Request, res: Response, next: NextFunction) => {
        const params: IUserListParams = {};
        const options: IPaginationOptions = {
            limit: 25,
            page: 1,
            sort: [['firstName', 'ASC'], ['lastName', 'ASC']],
        };
        if (req.query.page) {
            options.page = parseInt(req.query.page,  10);
        }
        if (req.query.limit) {
            options.limit = parseInt(req.query.limit,  10);
        }
        if (req.query.sort) {
            options.sort = JSON.parse(req.query.sort);
        }
        if (req.query.role === 'No Access') {
            params.roleId = null;
        } else if (req.query.role) {
            params.roleId = req.query.role;
        }
        return resolvers.user.list(params, options).then((users) => {
            return res.json(users);
        }).catch(next);
    });
    
    /**
     * @api {get} /users/:id Get user by id
     * @apiGroup User
     * @apiName Get user by id
     * @apiSuccess {Object} user User
     * @apiVersion 2.0.0
     */
    router.get('/:id', (req: Request, res: Response, next: NextFunction) => {
        return resolvers.user.get(req.params.id).then((user) => {
            return res.json(user);
        }).catch(next);
    });
    
    /**
     * @api {post} /users Create user
     * @apiGroup User
     * @apiName Create user
     * @apiSuccess {Object} user User
     * @apiVersion 2.0.0
     */
    router.post('/', (req: Request, res: Response, next: NextFunction) => {
        return resolvers.user.create(req.body).then((user) => {
            return resolvers.log.create({
                action: 'create',
                data: {},
                entityId: user.id,
                model: 'user',
                type: 'info',
                updates: compact(map(user.toJSON(), (newValue, fieldName) => {
                    switch (fieldName) {
                        case 'createdAt':
                        case 'id':
                        case 'name':
                        case 'password':
                        case 'updatedAt':
                            break;
                        default:
                            return {
                                fieldName,
                                newValue,
                                oldValue: 'null',
                            };
                    }
                })),
                userId: req.user.id,
            }).then(() => res.json(user));
        }).catch(next);
    });
    
    /**
     * @api {put} /users/:id Update user by Id
     * @apiGroup User
     * @apiName Update user by Id
     * @apiSuccess {Object} user User
     * @apiVersion 2.0.0
     */
    router.put('/:id', [
        body('roleId').optional().trim().escape().not().isEmpty().withMessage('The role id field is required'),
        body('roleId').optional().isUUID().custom((value) => resolvers.role.get(value)).withMessage(
            'The selected role does not exist',
        ),
        body('verified').optional().trim().escape().not().isEmpty().withMessage('The verified field is required'),
        body('verified').optional().customSanitizer(booleanSanitizer).isBoolean().withMessage(
            'The verified field must be a boolean',
        ),
    ], (req: Request, res: Response, next: NextFunction) => {
        const errors = validationResult(req).formatWith(formatErrors).mapped();
        const permission = find(req.user.role.permissions, ['name', 'Users']);
        if (!permission || !permission.modify) {
            return next({
                message: 'You do not have permission',
                statusCode: 403,
            });
        } else if (isEqual(req.user.id, req.params.id)) {
            return next({
                message: 'You are not permitted to update yourself',
                statusCode: 403,
            });
        }
        if (!isEmpty(errors)) {
            return next({
                errors,
                message: '422 Unprocessable Entity',
                statusCode: 422,
            });
        }
        if (isEmpty(req.body)) {
            return next({
                message: 'No fields have been changed',
                statusCode: 422,
            });
        }
        return resolvers.user.get(req.params.id).then((original) => {
            const data: IUserUpdateParams = {};
            forEach(req.body, (newValue, fieldName) => {
                switch (fieldName) {
                    case 'enabled':
                    case 'roleId':
                    case 'verified':
                        if (!isEqual(original.get(fieldName), newValue)) {
                            data[fieldName] = newValue;
                        }
                        break;
                    default:
                }
            });
            return resolvers.user.update(req.params.id, Object.assign({}, original, data)).then((user) => {
                return resolvers.log.create({
                    action: 'update',
                    data: {},
                    entityId: user.id,
                    model: 'user',
                    type: 'info',
                    updates: compact(map(original.toJSON(), (oldValue, fieldName) => {
                        switch (fieldName) {
                            case 'createdAt':
                            case 'id':
                            case 'name':
                            case 'password':
                            case 'updatedAt':
                                break;
                            default:
                                if (!isEqual(oldValue, user.get(fieldName))) {
                                    return {
                                        fieldName,
                                        newValue: user.get(fieldName),
                                        oldValue,
                                    };
                                }
                        }
                    })),
                    userId: req.user.id,
                }).then(() => res.json(user));
            });
        }).catch(next);
    });
    
    return router;
};
