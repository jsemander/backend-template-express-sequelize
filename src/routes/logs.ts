import { NextFunction, Request, Response, Router } from 'express';
import * as moment from 'moment-timezone';
import * as sequelize from 'sequelize';
import { IAppConfig } from '../app';
import { IPaginationOptions } from '../lib/Types';
import { IResolvers } from '../resolvers';
import { ILogListParams } from '../resolvers/log';

export default (configs: IAppConfig, resolvers: IResolvers) => {
    const router = Router();
    
    /**
     * @api {get} /logs List Paginated Logs
     * @apiGroup Log
     * @apiName List Paginated Logs
     * @apiSuccess {Object[]} logs Array of logs
     * @apiVersion 2.0.0
     */
    router.get('/', (req: Request, res: Response, next: NextFunction) => {
        const params: ILogListParams = {
            createdAt: {
                [sequelize.Op.gte]: moment().tz(configs.Timezone).startOf('day').toISOString(),
                [sequelize.Op.lte]: moment().tz(configs.Timezone).endOf('day').toISOString(),
            },
        };
        const options: IPaginationOptions = {
            limit: 25,
            page: 1,
            sort: [['createdAt', 'ASC']],
        };
        if (req.query.page) {
            options.page = parseInt(req.query.page, 10);
        }
        if (req.query.limit) {
            options.limit = parseInt(req.query.limit, 10);
        }
        if (req.query.sort) {
            options.sort = JSON.parse(req.query.sort);
        }
        if (req.query.start && req.query.end) {
            params.createdAt[sequelize.Op.gte] = req.query.start;
            params.createdAt[sequelize.Op.lte] = req.query.end;
        }
        if (req.query.action) {
            params.action = req.query.action;
        }
        if (req.query.model) {
            params.model = req.query.model;
        }
        if (req.query.level) {
            params.level = req.query.level;
        }
        if (req.query.type) {
            params.type = req.query.type;
        }
        return resolvers.log.list(params, options).then((pagination: any) => {
            return Promise.all(pagination.data.map((object: any) => {
                const log: { [key: string]: any; } = object.toJSON();
                return Promise.resolve().then(() => {
                    if (!log.user) {
                        /**
                         * Set User to API if empty
                         */
                        log.user = {
                            name: 'API',
                        };
                    }
                    /**
                     * Process Entity Property
                     */
                    if (log.entityId && log.model) {
                        switch (log.model) {
                            case 'role':
                                return resolvers.role.get(log.entityId).then((obj) => obj.toJSON());
                            case 'user':
                                return resolvers.user.get(log.entityId).then((obj) => obj.toJSON());
                            default:
                        }
                    }
                    return Promise.resolve({
                        name: 'Entity Not Found',
                    });
                }).then((entity: any) => {
                    log.entity = entity;
                    return log;
                }).catch(() => {
                    log.entity = {
                        name: 'Entity Not Found',
                    };
                    return log;
                });
            })).then((docs) => {
                pagination.data = docs;
                return pagination;
            });
        }).then((pagination) => res.json(pagination)).catch(next);
    });
    
    return router;
};
