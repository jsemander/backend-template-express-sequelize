import * as bcrypt from 'bcrypt';
import { NextFunction, Request, Response, Router } from 'express';
import { body, validationResult } from 'express-validator/check';
import { compact, forEach, isEmpty, isEqual, map } from 'lodash';
import { Authorization, formatErrors } from '.';
import { IAppConfig } from '../app';
import { IResolvers } from '../resolvers';
import { IUserCreateParams } from '../resolvers/user';

export default (configs: IAppConfig, resolvers: IResolvers) => {
    const router = Router();
    
    /**
     * @api {get} /me Get logged in user
     * @apiVersion 0.0.0
     * @apiSampleRequest /me
     * @apiGroup Authentication
     * @apiHeader {String} Authorization="Bearer [token]" token
     * @apiName Get logged in user
     * @apiError {Object[]} [errors] Input errors
     * @apiError {string} message General error message
     * @apiError {number} statusCode HTTP Status Code
     * @apiSuccess {string} id The id of the user
     * @apiSuccess {string} code The code of the user
     * @apiSuccess {boolean} deleted Whether the user is deleted
     * @apiSuccess {string} email The email of the user
     * @apiSuccess {boolean} enabled Whether the user is enabled
     * @apiSuccess {string} firstName The first name of the user
     * @apiSuccess {string} lastName The last name of the user
     * @apiSuccess {string} name The full name of the user
     * @apiSuccess {Object} role The role of the user
     * @apiSuccess {string} role.id The id of the role
     * @apiSuccess {boolean} role.enabled Whether the role is enabled
     * @apiSuccess {string} role.name The name of the role
     * @apiSuccess {Object[]} role.permissions The permissions of the role
     * @apiSuccess {string} role.permission.name The name of the permission
     * @apiSuccess {boolean} role.permission.access Wheter the role can access model data
     * @apiSuccess {boolean} role.permission.create Wheter the role can create a new model record
     * @apiSuccess {string} role.permission.level The level the role can create/read/update/delete
     * @apiSuccess {string} role.permission.model The model the role can create/read/update/delete
     * @apiSuccess {boolean} role.permission.modify Wheter the role can update a model record
     * @apiSuccess {string} role.permission.name The name of the permission
     * @apiSuccess {boolean} role.permission.state Wheter the role can delete a model record
     */
    router.get('/me', Authorization(resolvers), (req: Request, res: Response) => res.json(req.user));
    
    /**
     * @api {post} /forgotPassword Triggers a forgot password request
     * @apiVersion 0.0.0
     * @apiSampleRequest /forgotPassword
     * @apiGroup Authentication
     * @apiName Triggers a forgot password request
     * @apiParam (body) {string} email Email of the user
     * @apiError {Object[]} [errors] Input errors
     * @apiError {string} errors.email Error message for email
     * @apiError {string} errors.firstName Error message for firstName
     * @apiError {string} errors.lastName Error message for lastName
     * @apiError {string} message General error message
     * @apiError {number} statusCode HTTP Status Code
     * @apiSuccess {boolean} ok Acknowledge that the forgot password request was sent
     */
    router.post('/forgotPassword', [
        body('email').trim().escape().not().isEmpty().withMessage('The email field is required'),
        body('email').isEmail().withMessage('The email field should be a valid email address'),
    ], (req: Request, res: Response, next: NextFunction) => {
        const errors = validationResult(req).formatWith(formatErrors).mapped();
        if (!isEmpty(errors)) {
            return next({
                errors,
                message: '422 Unprocessable Entity',
                statusCode: 422,
            });
        }
        return resolvers.user.getByEmail(req.body.email).then((user) => {
            // Check if the record was found
            if (!user) {
                return Promise.reject<any>({
                    message: 'You do not have permission to request a password reset',
                    statusCode: 403,
                });
            }
            // Check if user is verified
            if (!user.verified) {
                /**
                 * Send Verification Email
                 */
                return resolvers.session.create(user.id).then((session) => {
                    return resolvers.log.create({
                        action: 'send',
                        data: {},
                        entity: {
                            name: 'Email Sent to User',
                        },
                        model: 'email',
                        type: 'info',
                        updates: [],
                    });
                    // return resolvers.mail.sendVerification([{
                    //     email: user.email,
                    //     name: user.name,
                    // }], {
                    //     token: resolvers.jwt.createToken(session.id),
                    // }).then(() => {
                    //     return resolvers.log.create({
                    //         action: 'send',
                    //         data: {},
                    //         entity: {
                    //             name: 'Email Sent to User',
                    //         },
                    //         model: 'email',
                    //         type: 'info',
                    //         updates: [],
                    //     });
                    // }).catch((err) => {
                    //     return resolvers.log.create({
                    //         action: 'send',
                    //         data: {},
                    //         entity: {
                    //             name: 'Email Error',
                    //         },
                    //         error: err,
                    //         model: 'email',
                    //         type: 'error',
                    //         updates: [],
                    //     });
                    // });
                }).then(() => {
                    return Promise.reject<any>({
                        message: 'You must verify your email address',
                        statusCode: 403,
                    });
                });
            }
            return resolvers.user.isAuthorized(user.id, false).then(() => {
                return resolvers.session.create(user.id).then((session) => {
                    return resolvers.log.create({
                        action: 'send',
                        data: {},
                        entity: {
                            name: 'Email Sent to User',
                        },
                        model: 'email',
                        type: 'info',
                        updates: [],
                    }).then(() => res.json({ ok: true }));
                    // return resolvers.mail.sendPasswordReset([{
                    //     email: user.email,
                    //     name: user.name,
                    // }], {
                    //     token: resolvers.jwt.createToken(session.id),
                    // }).then(() => res.json({ ok: true })).then(() => {
                    //     return resolvers.log.create({
                    //         action: 'send',
                    //         data: {},
                    //         entity: {
                    //             name: 'Email Sent to User',
                    //         },
                    //         model: 'email',
                    //         type: 'info',
                    //         updates: [],
                    //     });
                    // }).catch((err) => {
                    //     return resolvers.log.create({
                    //         action: 'send',
                    //         data: {},
                    //         entity: {
                    //             name: 'Email Error',
                    //         },
                    //         error: err,
                    //         model: 'email',
                    //         type: 'error',
                    //         updates: [],
                    //     });
                    // });
                });
            });
        }).catch(next);
    });
    
    /**
     * @api {post} /login Authenticate user
     * @apiVersion 0.0.0
     * @apiSampleRequest /login
     * @apiGroup Authentication
     * @apiName Authenticate user
     * @apiParam (body) {string} password Password of the user
     * @apiParam (body) {string} username Username of the user
     * @apiError {Object[]} [errors] Input errors
     * @apiError {string} errors.username Error message for username
     * @apiError {string} errors.password Error message for password
     * @apiError {string} message General error message
     * @apiError {number} statusCode HTTP Status Code
     * @apiSuccess {boolean} ok Acknowledge that the user was authenticated
     */
    router.post('/login', [
        body('password').trim().escape().not().isEmpty().withMessage('The password field is required'),
        body('password').custom((value) => {
            return value.length >= configs.Password.minLength;
        }).withMessage(`The password field must be at least ${configs.Password.minLength} charcters`),
        body('username').trim().escape().not().isEmpty().withMessage('The username field is required'),
        body('username').isEmail().withMessage('The username field should be a valid email address'),
    ], (req: Request, res: Response, next: NextFunction) => {
        const errors = validationResult(req).formatWith(formatErrors).mapped();
        if (!isEmpty(errors)) {
            return next({
                errors,
                message: '422 Unprocessable Entity',
                statusCode: 422,
            });
        }
        return resolvers.user.getByUsername(req.body.username).then((user) => {
            // Check if the record was found
            if (!user) {
                return Promise.reject<any>({
                    message: 'Your username/password combination is incorrect',
                    statusCode: 403,
                });
            }
            // Check if user is verified
            if (!user.verified) {
                /**
                 * Send Verification Email
                 */
                return resolvers.session.create(user.id).then((session) => {
                    return resolvers.log.create({
                        action: 'send',
                        data: {},
                        entity: {
                            name: 'Email Sent to User',
                        },
                        model: 'email',
                        type: 'info',
                        updates: [],
                    });
                    // return resolvers.mail.sendVerification([{
                    //     email: user.email,
                    //     name: user.name,
                    // }], {
                    //     token: resolvers.jwt.createToken(session.id),
                    // }).then(() => {
                    //     return resolvers.log.create({
                    //         action: 'send',
                    //         data: {},
                    //         entity: {
                    //             name: 'Email Sent to User',
                    //         },
                    //         model: 'email',
                    //         type: 'info',
                    //         updates: [],
                    //     });
                    // }).catch((err) => {
                    //     return resolvers.log.create({
                    //         action: 'send',
                    //         data: {},
                    //         entity: {
                    //             name: 'Email Error',
                    //         },
                    //         error: err,
                    //         model: 'email',
                    //         type: 'error',
                    //         updates: [],
                    //     });
                    // });
                }).then(() => {
                    return Promise.reject<any>({
                        message: 'You must verify your email address',
                        statusCode: 403,
                    });
                });
            }
            return resolvers.user.isAuthorized(user.id, false).then(() => {
                if (bcrypt.compareSync(req.body.password + configs.Authentication.salt, user.password)) {
                    return resolvers.session.create(user.id).then((session) => {
                        return res.json({
                            token: resolvers.jwt.createToken(session.id),
                        });
                    });
                }
                return Promise.reject<any>({
                    message: 'Your username/password combination is incorrect',
                    statusCode: 403,
                });
            });
        }).catch(next);
    });
    
    /**
     * @api {post} /register Register new affiliate user
     * @apiVersion 0.0.0
     * @apiSampleRequest /register
     * @apiGroup Authentication
     * @apiName Register new affiliate user
     * @apiParam (body) {string} email Email of the user
     * @apiParam (body) {string} firstName First Name of the user
     * @apiParam (body) {string} lastName Last Name of the user
     * @apiParam (body) {string} password Password of the user
     * @apiError {Object[]} [errors] Input errors
     * @apiError {string} errors.email Error message for email
     * @apiError {string} errors.firstName Error message for first name
     * @apiError {string} errors.lastName Error message for last name
     * @apiError {string} errors.password Error message for password
     * @apiError {string} message General error message
     * @apiError {number} statusCode HTTP Status Code
     * @apiSuccess {boolean} ok Acknowledge that the user was created
     */
    router.post('/register', [
        body('email').trim().escape().not().isEmpty().withMessage('The email field is required'),
        body('email').isEmail().withMessage('The email field should be a valid email address'),
        body('firstName').trim().escape().not().isEmpty().withMessage('The first name field is required'),
        body('lastName').trim().escape().not().isEmpty().withMessage('The last name field is required'),
        body('password').trim().escape().not().isEmpty().withMessage('The password field is required'),
        body('password').custom((value) => {
            return value.length >= configs.Password.minLength;
        }).withMessage(`The password field must be at least ${configs.Password.minLength} charcters`),
    ], (req: Request, res: Response, next: NextFunction) => {
        return resolvers.role.getByName('User').then((role) => {
            const data: IUserCreateParams = {
                email: '',
                firstName: '',
                lastName: '',
                password: '',
                roleId: '',
                verified: true,
            };
            const errors = validationResult(req).formatWith(formatErrors).mapped();
            if (!isEmpty(errors)) {
                return Promise.reject<any>({
                    errors,
                    message: '422 Unprocessable Entity',
                    statusCode: 422,
                });
            }
            forEach(req.body, (newValue, fieldName) => {
                switch (fieldName) {
                    case 'email':
                    case 'firstName':
                    case 'lastName':
                        data[fieldName] = newValue;
                        break;
                    default:
                }
            });
            return resolvers.user.create(Object.assign(data, {
                password: bcrypt.hashSync(
                    req.body.password + configs.Authentication.salt,
                    configs.Authentication.saltOrRounds,
                ),
                roleId: role.id,
            })).then((createdObject) => {
                return resolvers.log.create({
                    action: 'register',
                    data: {},
                    entityId: createdObject.id,
                    model: 'user',
                    type: 'info',
                    updates: compact(map(createdObject.toJSON(), (newValue, fieldName) => {
                        switch (fieldName) {
                            case 'createdAt':
                            case 'id':
                            case 'name':
                            case 'password':
                            case 'updatedAt':
                                break;
                            default:
                                return {
                                    fieldName,
                                    newValue,
                                    oldValue: null,
                                };
                        }
                    })),
                    userId: createdObject.id,
                }).then(() => res.json({ ok: true })).then(() => {
                    return resolvers.session.create(createdObject.id).then((session) => {
                        return resolvers.log.create({
                            action: 'send',
                            data: {},
                            entity: {
                                name: 'Email Sent to User',
                            },
                            model: 'email',
                            type: 'info',
                            updates: [],
                        });
                        // return resolvers.mail.sendVerification([{
                        //     email: createdObject.email,
                        //     name: createdObject.name,
                        // }], {
                        //     token: resolvers.jwt.createToken(session.id),
                        // }).then(() => {
                        //     return resolvers.log.create({
                        //         action: 'send',
                        //         data: {},
                        //         entity: {
                        //             name: 'Email Sent to User',
                        //         },
                        //         model: 'email',
                        //         type: 'info',
                        //         updates: [],
                        //     });
                        // }).catch((err) => {
                        //     return resolvers.log.create({
                        //         action: 'send',
                        //         data: {},
                        //         entity: {
                        //             name: 'Email Error',
                        //         },
                        //         error: err.error,
                        //         model: 'email',
                        //         type: 'error',
                        //         updates: [],
                        //     });
                        // });
                    });
                });
            });
        }).catch(next);
    });
    
    /**
     * @api {put} /me Update logged in user account
     * @apiVersion 0.0.0
     * @apiSampleRequest /me
     * @apiGroup User
     * @apiHeader {String} Authorization="Bearer [token]" token
     * @apiName Update logged in user account
     * @apiParam (body) {string} [currentPassword] The current password for the user, if not sent the password field will be bypassed
     * @apiParam (body) {string} [email] Email of the user
     * @apiParam (body) {string} [firstName] First Name of the user
     * @apiParam (body) {string} [lastName] Last Name of the user
     * @apiParam (body) {string} [password] Password of the user
     * @apiError {Object[]} [errors] Input errors
     * @apiError {string} message General error message
     * @apiError {number} statusCode HTTP Status Code
     * @apiSuccess {string} id The id of the user
     * @apiSuccess {string} email The email of the user
     * @apiSuccess {boolean} enabled Whether the user is enabled
     * @apiSuccess {string} firstName The first name of the user
     * @apiSuccess {string} lastName The last name of the user
     * @apiSuccess {string} name The full name of the user
     * @apiSuccess {Object} role The role of the user
     * @apiSuccess {string} role.id The id of the role
     * @apiSuccess {boolean} role.enabled Whether the role is enabled
     * @apiSuccess {string} role.name The name of the role
     * @apiSuccess {Object[]} role.permissions The permissions of the role
     * @apiSuccess {string} role.permission.name The name of the permission
     * @apiSuccess {boolean} role.permission.access Wheter the role can access model data
     * @apiSuccess {boolean} role.permission.create Wheter the role can create a new model record
     * @apiSuccess {string} role.permission.level The level the role can create/read/update/delete
     * @apiSuccess {string} role.permission.model The model the role can create/read/update/delete
     * @apiSuccess {boolean} role.permission.modify Wheter the role can update a model record
     * @apiSuccess {string} role.permission.name The name of the permission
     * @apiSuccess {boolean} role.permission.state Wheter the role can delete a model record
     */
    router.put('/me', Authorization(resolvers), [
        body('currentPassword').optional().trim().escape().not().isEmpty().withMessage(
            'The current password field is required',
        ),
        body('currentPassword').optional().custom((value) => {
            return value.length >= configs.Password.minLength;
        }).withMessage(`The current password field must be at least ${configs.Password.minLength} charcters`),
        body('email').optional().trim().escape().not().isEmpty().withMessage('The email field is required'),
        body('email').optional().isEmail().withMessage('The email field should be a valid email address'),
        body('firstName').optional().trim().escape().not().isEmpty().withMessage('The first name field is required'),
        body('lastName').optional().trim().escape().not().isEmpty().withMessage('The last name field is required'),
        body('password').optional().trim().escape().not().isEmpty().withMessage('The password field is required'),
        body('password').optional().custom((value) => {
            return value.length >= configs.Password.minLength;
        }).withMessage(`The password field must be at least ${configs.Password.minLength} charcters`),
    ], (req: Request, res: Response, next: NextFunction) => {
        const data: { [key: string]: any; } = {};
        const errors = validationResult(req).formatWith(formatErrors).mapped();
        if (!isEmpty(errors)) {
            return next({
                errors,
                message: '422 Unprocessable Entity',
                statusCode: 422,
            });
        }
        if (isEmpty(req.body)) {
            return next({
                message: 'No fields have been changed',
                statusCode: 422,
            });
        }
        if (req.body.currentPassword) {
            // Validate if the current password matches
            if (
                req.body.password &&
                (
                    !req.body.currentPassword ||
                    !bcrypt.compareSync(
                        req.body.currentPassword + configs.Authentication.salt,
                        req.user.password,
                    )
                )
            ) {
                return next({
                    message: 'Your current password is incorrect',
                    statusCode: 422,
                });
            } else if (
                bcrypt.compareSync(
                    req.body.password + configs.Authentication.salt,
                    req.user.password,
                )
            ) {
                return next({
                    message: 'You cannot supply the same password as your current password',
                    statusCode: 422,
                });
            }
            // Set password field
            data.password = bcrypt.hashSync(
                req.body.password + configs.Authentication.salt,
                configs.Authentication.saltOrRounds,
            );
        }
        forEach(req.body, (newValue, fieldName) => {
            if (!isEqual(req.user.get(fieldName), newValue)) {
                switch (fieldName) {
                    case 'email':
                        data.email = newValue;
                        data.verified = false;
                        break;
                    case 'firstName':
                    case 'lastName':
                        data[fieldName] = newValue;
                        break;
                    default:
                }
            }
        });
        if (isEmpty(data)) {
            return next({
                message: 'No fields have been changed',
                statusCode: 422,
            });
        }
        return resolvers.user.update(req.user.id, data).then((user) => {
            return resolvers.log.create({
                action: 'update',
                data: {},
                entityId: user.id,
                model: 'user',
                type: 'info',
                updates: compact(map(req.user.toJSON(), (oldValue, fieldName) => {
                    switch (fieldName) {
                        case 'createdAt':
                        case 'id':
                        case 'name':
                        case 'password':
                        case 'updatedAt':
                            break;
                        default:
                            if (!isEqual(oldValue, user.get(fieldName))) {
                                return {
                                    fieldName,
                                    newValue: user.get(fieldName),
                                    oldValue,
                                };
                            }
                    }
                })),
                userId: req.user.id,
            }).then(() => res.json(user)).then(() => {
                if (!user.verified) {
                    return resolvers.session.create(user.id).then((session) => {
                        return resolvers.log.create({
                            action: 'send',
                            data: {},
                            entity: {
                                name: 'Email Sent to User',
                            },
                            model: 'email',
                            type: 'info',
                            updates: [],
                        });
                        // return resolvers.mail.sendVerification([{
                        //     email: user.email,
                        //     name: user.name,
                        // }], {
                        //     token: resolvers.jwt.createToken(session.id),
                        // }).then(() => {
                        //     return resolvers.log.create({
                        //         action: 'send',
                        //         data: {},
                        //         entity: {
                        //             name: 'Email Sent to User',
                        //         },
                        //         model: 'email',
                        //         type: 'info',
                        //         updates: [],
                        //     });
                        // }).catch((err) => {
                        //     return resolvers.log.create({
                        //         action: 'send',
                        //         data: {},
                        //         entity: {
                        //             name: 'Email Error',
                        //         },
                        //         model: 'email',
                        //         error: err,
                        //         type: 'error',
                        //         updates: [],
                        //     });
                        // });
                    });
                } else {
                    return Promise.resolve<any>({});
                }
            });
        }).catch(next);
    });
    
    /**
     * @api {put} /resetPassword Reset the user password
     * @apiVersion 0.0.0
     * @apiSampleRequest /resetPassword
     * @apiGroup Authentication
     * @apiHeader {String} Authorization="Bearer [token]" token
     * @apiName Reset the user password
     * @apiParam (body) {string} password The new password for the user
     * @apiError {Object[]} [errors] Input errors
     * @apiError {string} message General error message
     * @apiError {number} statusCode HTTP Status Code
     * @apiSuccess {string} token Authentication Token
     */
    router.put('/resetPassword', Authorization(resolvers), [
        body('password').trim().escape().not().isEmpty().withMessage('The password field is required'),
        body('password').optional().custom((value) => {
            return value.length >= configs.Password.minLength;
        }).withMessage(`The password field must be at least ${configs.Password.minLength} charcters`),
    ], (req: Request, res: Response, next: NextFunction) => {
        const errors = validationResult(req).formatWith(formatErrors).mapped();
        if (!isEmpty(errors)) {
            return next({
                errors,
                message: '422 Unprocessable Entity',
                statusCode: 422,
            });
        }
        const data = {
            password: bcrypt.hashSync(
                req.body.password + configs.Authentication.salt,
                configs.Authentication.saltOrRounds,
            ),
        };
        // Update user password and password history
        return resolvers.user.update(req.user.id, data).then(() => {
            return resolvers.log.create({
                action: 'resetPassword',
                data: {},
                entityId: req.user.id,
                model: 'user',
                type: 'info',
                updates: [],
            }).then(() => {
                return resolvers.session.create(req.user.id).then((session) => {
                    return res.json({
                        token: resolvers.jwt.createToken(session.id),
                    });
                });
            });
        }).catch(next);
    });
    
    /**
     * @api {put} /verify Verify the email address of the user
     * @apiVersion 0.0.0
     * @apiSampleRequest /verify
     * @apiGroup Authentication
     * @apiHeader {String} Authorization="Bearer [token]" token
     * @apiName Verify the email address of the user
     * @apiError {Object[]} [errors] Input errors
     * @apiError {string} message General error message
     * @apiError {number} statusCode HTTP Status Code
     * @apiSuccess {string} token Authentication Token
     */
    router.put('/verify', Authorization(resolvers), (req: Request, res: Response, next: NextFunction) => {
        if (req.user.verified) {
            return next({
                message: 'Your user account is already verified',
                statusCode: 409,
            });
        }
        return resolvers.user.update(req.user.id, {
            verified: true,
        }).then(() => {
            return resolvers.log.create({
                action: 'verify',
                data: {},
                entityId: req.user.id,
                model: 'user',
                type: 'info',
                updates: [],
            }).then(() => {
                return resolvers.session.removeByUser(req.user.id).then(() => {
                    return res.json({ ok: true });
                });
            });
        }).catch(next);
    });
    
    return router;
};
