import { NextFunction, Request, Response, Router } from 'express';
import { body, validationResult } from 'express-validator/check';
import { compact, filter, find, forEach, isEmpty, isEqual, map } from 'lodash';
import { formatErrors } from '.';
import { IPaginationOptions } from '../lib/Types';
import { IResolvers } from '../resolvers';
import { IRoleListParams, IRoleUpdateParams } from '../resolvers/role';

export default (resolvers: IResolvers) => {
    const router = Router();
    
    /**
     * @api {get} /roles List Paginated Roles
     * @apiGroup Role
     * @apiName List Paginated Roles
     * @apiSuccess {Object[]} roles Array of roles
     * @apiVersion 2.0.0
     */
    router.get('/', (req: Request, res: Response, next: NextFunction) => {
        const params: IRoleListParams = {};
        const options: IPaginationOptions = {
            limit: 25,
            page: 1,
            sort: [['name', 'ASC']],
        };
        if (req.query.page) {
            options.page = parseInt(req.query.page,  10);
        }
        if (req.query.limit) {
            options.limit = parseInt(req.query.limit,  10);
        }
        if (req.query.sort) {
            options.sort = JSON.parse(req.query.sort);
        }
        if (req.query.status) {
            params.enabled = (req.query.status === 'Enabled');
        }
        return resolvers.role.list(params, options).then((roles) => {
            return res.json(roles);
        }).catch(next);
    });
    
    /**
     * @api {get} /roles/:id Get role by id
     * @apiGroup Role
     * @apiName Get role by id
     * @apiSuccess {Object} role Role
     * @apiVersion 2.0.0
     */
    router.get('/:id', (req: Request, res: Response, next: NextFunction) => {
        return resolvers.role.get(req.params.id).then((role) => {
            return res.json(role);
        }).catch(next);
    });
    
    /**
     * @api {post} /roles Create role
     * @apiGroup Role
     * @apiName Create role
     * @apiSuccess {Object} role Role
     * @apiVersion 2.0.0
     */
    router.post('/', (req: Request, res: Response, next: NextFunction) => {
        return resolvers.role.create(req.body).then((role) => {
            return resolvers.log.create({
                action: 'create',
                data: {},
                entityId: role.id,
                model: 'role',
                type: 'info',
                updates: compact(map(role.toJSON(), (newValue, fieldName) => {
                    switch (fieldName) {
                        case 'createdAt':
                        case 'id':
                        case 'updatedAt':
                            break;
                        default:
                            return {
                                fieldName,
                                newValue,
                                oldValue: 'null',
                            };
                    }
                })),
                userId: req.user.id,
            }).then(() => res.json(role));
        }).catch(next);
    });
    
    /**
     * @api {put} /roles/:id Update role by id
     * @apiGroup Role
     * @apiName Update role by Id
     * @apiSuccess {Object} role Role
     * @apiVersion 2.0.0
     */
    router.put('/:id', [
        body('name').optional().trim().escape().not().isEmpty().withMessage('The name field is required'),
    ], (req: Request, res: Response, next: NextFunction) => {
        const errors = validationResult(req).formatWith(formatErrors).mapped();
        const permission = find(req.user.role.permissions, ['name', 'Roles']);
        if (!permission || !permission.modify) {
            return next({
                message: 'You do not have permission',
                statusCode: 403,
            });
        }
        if (!isEmpty(errors)) {
            return next({
                errors,
                message: '422 Unprocessable Entity',
                statusCode: 422,
            });
        }
        if (isEmpty(req.body)) {
            return next({
                message: 'No fields have been changed',
                statusCode: 422,
            });
        }
        return resolvers.role.get(req.params.id).then((original) => {
            const data: IRoleUpdateParams = {};
            forEach(req.body, (newValue, fieldName) => {
                switch (fieldName) {
                    case 'name':
                        if (!isEqual(original.get(fieldName), newValue)) {
                            data[fieldName] = newValue;
                        }
                        break;
                    default:
                }
            });
            if (isEmpty(data)) {
                return Promise.reject<any>({
                    message: 'No fields have been changed',
                    statusCode: 422,
                });
            }
            return resolvers.role.update(req.params.id, Object.assign({}, original, data)).then((role) => {
                // TODO: User role.changed() to get keys that changed
                return resolvers.log.create({
                    action: 'update',
                    data: {},
                    entityId: role.id,
                    model: 'role',
                    type: 'info',
                    updates: compact(map(original.toJSON(), (oldValue, fieldName) => {
                        switch (fieldName) {
                            case 'createdAt':
                            case 'id':
                            case 'updatedAt':
                                break;
                            // case 'permissions':
                            //     return {
                            //         fieldName,
                            //         newValue: filter(role.permissions, (value: PermissionModel) => {
                            //             const found: { [key: string]: any; } | undefined = find((oldValue as PermissionModel).toJSON(), ['name', value.name]);
                            //             if (found) {
                            //                 let changed = false;
                            //                 forEach(value, (keyValue, key) => {
                            //                     if (!changed) {
                            //                         changed = !isEqual(found[key], keyValue);
                            //                     }
                            //                 });
                            //                 return changed;
                            //             }
                            //             return true;
                            //         }),
                            //         oldValue: filter(oldValue, (value: PermissionModel) => {
                            //             const found: { [key: string]: any; } | undefined = find(role.get(fieldName), ['name', value.name]);
                            //             if (found) {
                            //                 let changed = false;
                            //                 forEach(value, (keyValue, key) => {
                            //                     if (!changed) {
                            //                         changed = !isEqual(keyValue, found[key]);
                            //                     }
                            //                 });
                            //                 return changed;
                            //             }
                            //             return true;
                            //         }),
                            //     };
                            default:
                                if (!isEqual(oldValue, role.get(fieldName))) {
                                    return {
                                        fieldName,
                                        newValue: role.get(fieldName),
                                        oldValue,
                                    };
                                }
                        }
                    })),
                    userId: req.user.id,
                }).then(() => res.json(role));
            });
        }).catch(next);
    });
    
    return router;
};
