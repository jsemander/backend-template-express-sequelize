import { Model, ModelCtor } from 'sequelize';
import { IPagination } from './Types';

/**
 * Calculates the skip amount
 */
export const calculateSkip = (page: number, limit: number): number => (page - 1) * limit;

/**
 * Paginates the records
 */
export function paginateRecords<T extends Model<any, any>>(
    model: ModelCtor<T>,
    params: any,
    include: any[],
    sort: any,
    page: number,
    limit: number,
): Promise<IPagination<T>> {
    return model.findAndCountAll<T>({
        include,
        limit,
        offset: calculateSkip(page, limit),
        order: sort,
        where: params,
    }).then((result) => {
        return {
            data: result.rows,
            meta: {
                pagination: {
                    count: result.rows.length,
                    total: result.count,
                },
            },
        };
    });
}
