import * as sequelize from 'sequelize';

export interface IDateRange {
    [sequelize.Op.eq]?: string;
    [sequelize.Op.gt]?: string;
    [sequelize.Op.gte]?: string;
    [sequelize.Op.lt]?: string;
    [sequelize.Op.lte]?: string;
}
export interface IPagination<T> {
    data: T[];
    meta: {
        pagination: {
            count: number;
            total: number;
        };
    };
}
export interface IPaginationOptions {
    limit: number;
    page: number;
    sort: any;
}
export interface IPromiseError {
    message: string;
    statusCode: number;
}
export interface ITaskResponse {
    error?: string | object | Error;
    success: boolean;
}
