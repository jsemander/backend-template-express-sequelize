import { Request } from 'express';

/**
 * Converts boolean string values to boolean values
 */
export const booleanSanitizer = (value: string, options?: { req: Request; location: string; path: string }) => {
    if (value === '1' || value === 'true') {
        return true;
    } else if (value === '0' || value === 'false') {
        return false;
    } else {
        return value;
    }
};

/**
 * Converts number string values to number values
 */
export const numberSanitizer = (value: string, options?: { req: Request; location: string; path: string }) => {
    if (!isNaN(Number(value))) {
        return Number(value);
    }
    return value;
};

/**
 * Validates that the value supplied is a date
 */
export const dateValidator = (value: string, options?: { req: Request; location: string; path: string }) => {
    return /^[\d]{4}\-[\d]{2}\-[\d]{2}T[\d]{2}:[\d]{2}:[\d]{2}(|\.[\d]{3})(-[\d]{2}:[\d]{2}|Z)$/.test(`${value}`);
};

/**
 * Validates that the value supplied is a number
 */
export const numberValidator = (value: string, options?: { req: Request; location: string; path: string }) => {
    return /^[-\d\.]+$/.test(`${value}`) || !isNaN(Number(value));
};
